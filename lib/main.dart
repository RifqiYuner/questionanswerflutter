import 'package:flutter/material.dart';
import 'package:flutter_course1/question.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  var _questionIndex = 0;

  void _answerQuestion() {
    setState(() {
      _questionIndex++;
      if (_questionIndex == 2) {
        _questionIndex = 0;
      }
    });
    print('Answer Choosen!');
    print(_questionIndex);
  }

  @override
  Widget build(BuildContext context) {
    var questions = [
      "What's your favorite game developer?",
      "What's your favorite game tittle?",
    ];
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('My First App'),
        ),
        body: Column(
          children: [
            Question(questions[_questionIndex]),
            RaisedButton(
              child: Text('Answer 1'),
              onPressed: _answerQuestion,
            ),
            RaisedButton(
              child: Text('Answer 2'),
              onPressed: () {
                print('Answer 2 Choosen!');
              },
            ),
            RaisedButton(
              child: Text('Answer 3'),
              onPressed: () => print('Answer 3 Choosen!'),
            ),
          ],
        ),
      ),
    );
  }
}
